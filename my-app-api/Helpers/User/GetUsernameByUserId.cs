﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Net.Http;

namespace my_app_api.Controllers
{
    public class GetUsernameByUserId
    {
        public int user_id;
        public GetUsernameByUserId(int user_id)
        {
            this.user_id = user_id;

        }

        public UsernameModel getUsername()
        {
            try
            {
                SqlConnectionStringBuilder builder = WebApiConfig.Connection();
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();

                    sb.Append("SELECT " +
                     "[user_name] " +
                      "FROM [dbo].[users] " +
                              "WHERE [user_id] = " + user_id
                     );

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                string _username = "";
                                while (reader.Read())
                                {
                                    _username = reader.GetString(0);
                                }

                                if (_username == string.Empty)
                                {
                                    return new Username(false, "Internal error", "");
                                }

                                return new Username(true, _username);

                            }
                            else
                            {
                                return new Username(false, "", "");
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Select error: " + e.ToString());
                return new UsernameModel(false, e.ToString(), "");
            }
        }
    }
}