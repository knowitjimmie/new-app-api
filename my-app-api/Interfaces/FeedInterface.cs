﻿using System;
using System.Collections.Generic;

namespace my_app_api
{
    public interface FeedInterface
    {
        List<FeedModel> feed { get; set; }

        List<FeedModel> GetFeed(); // get all
        FeedModel GetFeed(int post_id); // specfic post/feed post
    }
}
