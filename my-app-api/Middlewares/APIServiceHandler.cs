﻿using System;
namespace my_app_api
{

    /*
     * This class shall act as a middleware for the base communication layer.
     * 
     * Controllers shall communicate with this layer instead of the services. 
     * 
     * This file shall pick the correct service to use.
     * 
     * why? It has its pro's to never incapsulate the service to the rest of the codebase. If a new server, doc based, gets introduced, then the app needs to be re-written to support it.
     * In this case, we'd only change the handlers target from a SQL based service to another.
     * 
     * 
     * 
     * This file shall be written as a heirarchy with the service handlers. 
     * 
     */
    public class APIServiceHandler
    {
        public Feed feed = new FeedInterface();
        public User user = new UserFeed();

        public APIServiceHandler()
        {
        }

        public List<Feed> getFeed() {

        }

        public List<User> getUsers() {

        }

        
    }
}
