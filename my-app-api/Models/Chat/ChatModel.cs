﻿using System;
namespace my_app_api
{
    public class ChatModel
    {
        public int chat_id { get; set; }
        public int chat_from_id { get; set; }
        public int chat_to_id { get; set; }
        public string chat_message { get; set; }
        public string chat_date { get; set; }

        public ChatModel(int chat_id, int chat_from_id, int chat_to_id, string chat_message, string chat_date)
        {
            this.chat_id = chat_id;
            this.chat_from_id = chat_from_id;
            this.chat_to_id = chat_to_id;
            this.chat_message = chat_message;
            this.chat_date = chat_date;
            return;
        }
    }
}
