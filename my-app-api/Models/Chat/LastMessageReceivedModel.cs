﻿using System;
namespace my_app_api
{
    public class LastMessageReceivedModel : Response
    {

        public string latest_message { get; set; }
        public string latest_message_date { get; set; }

        // contructor for returning an error.
        public LastMessageReceivedModel(Response response)
            : base(response.success, response.error, response.message)
        {
            this.latest_message = latest_message;
            this.latest_message_date = latest_message_date;
            return;
        }

        // contructor for returning the values. Re-use when sending to client?
        public LastMessageReceivedModel(Response response, string latest_message, string latest_message_date)
            : base(response.success, response.error, response.message)
        {
            this.latest_message = latest_message;
            this.latest_message_date = latest_message_date;
            return;
        }
    }
}
