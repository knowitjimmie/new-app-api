﻿using System;
namespace my_app_api
{
    public class NewChatModel
    {
        string username { get; set; }
        string to_username { get; set; }
        string message { get; set; }

        public NewChatModel(string username, string to_username, string message)
        {
            this.username = username;
            this.to_username = to_username;
            this.message = message;
            return;
        }
    }
}
