﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace my_app_api
{
    public class FeedModel : Response
    {
        public int post_id { get; set; }
        public int post_user_id { get; set; }

        public string post_content { get; set; }
        public string post_date { get; set; }

        public List<UserModel> post_likes { get; set; }
        public List<UserModel> post_dislikes { get; set; }

        public FeedModel(Response response, int post_id, int post_user_id, string post_content, string post_date, List<UserModel> post_likes, List<UserModel> post_dislikes)
            : base(response.success, response.error, response.message) {

        }

        public FeedModel(Response response)
          : base(response.success, response.error, response.message)
        {

        }
    }
}
