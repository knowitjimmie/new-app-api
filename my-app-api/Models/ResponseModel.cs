﻿using System;
namespace my_app_api
{

    /*
     * This class will act as the super for all kinds of models.
     * Whenever doing an action, returning data or whatever, the Response class shall be used to follow a "red thread"
     * 
     * Extend Response in other models and return X values, with also general functions such as "didSucceed" in this parent.
     */
    public class Response
    {
        public Boolean success { get; set; }
        public Boolean error { get; set; }
        public int integer { get; set; }
        public string message { get; set; }


        public Boolean didSucceed()
        {
            return success;
        }

        public Response(Boolean success, Boolean error)
        {
            this.success = success;
            this.error = error;
            return;
        }

        public Response(Boolean success, int integer)
        {
            this.success = success;
            this.integer = integer;
            return;
        }

        public Response(Boolean success, string message)
        {
            this.success = success;
            this.message = message;
            return;
        }

        public Response(Boolean success, Boolean error, string message)
        {
            this.success = success;
            this.error = error;
            this.message = message;
            return;
        }
    }
}
