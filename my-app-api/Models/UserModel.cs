﻿using System;
namespace my_app_api
{
    public class UserModel // Shall conform to a "user" interface.
    {
        // What should a user look like?
        // Which traits shall be implemented in the database table.

        Modules modules = new Modules();

        public int user_id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string full_name { 
            get {
                // shall have condition such as if "you" are logged in, otherwise the full_name shall not be visible.
                return this.full_name;
            }
            set {
                this.full_name = value;
            }
        }
        public string creation_date {
            get {
                // shall return a value as (21 days ago) for example.
                return modules.betweenDates(creation_date);
            }
        }

        // insert full data 
        public UserModel(int user_id, string username, string email, string full_name, string creation_date)
        {
            this.user_id = user_id;
            this.username = username;
            this.email = email;
            this.full_name = full_name;
            // creation date are set by SQL?
        }

        public Response deleteUser() {
            // do Query
        }

        public Response changePassword(string new_password, string old_password) {

        }

        public Response createNewUser() {

        }
    }
}
