﻿using System;
namespace myappapi
{
    public class UsernameModel
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string username { get; set; }

        public UsernameModel(bool success, string message, string username)
        {
            this.success = success;
            this.message = message;
            this.username = username;
            return;
        }

        public UsernameModel(bool success, string username)
        {
            this.success = success;
            this.username = username;
            return;
        }

        public bool didSucceed()
        {
            return success;
        }
    }
}
