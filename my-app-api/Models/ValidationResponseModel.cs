﻿using System;
namespace my_app_api
{
    public class ValidationResponse
    {
        public bool success { get; set; }
        public bool failure { get; set; }

        public string message { get; set; }

        public ValidationResponse(bool success, bool failure, string message)
        {
            this.success = success;
            this.failure = failure;
            this.message = message;
        }

        public ValidationResponse(bool success, string message)
        {
            this.success = success;
            this.message = message;
        }
    }
}
